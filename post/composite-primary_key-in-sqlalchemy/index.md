
When using fully declared table metadata,use the `primary_key=True` flag
on those columns:

```python
class Dummy(Base):
    __tablename__ = "dummy"

    pk1 = Column(Integer, primary_key=True)
    pk2 = Column(String, primary_key=True)
```

This will create the following table (in postgres) :

    testdb=> \d dummy;
                                  Table "public.dummy"
    Column |       Type        |                      Modifiers
    -------+-------------------+-----------------------------------------------------
    pk1    | integer           | not null default nextval('dummy_pk1_seq'::regclass)
    pk2    | character varying | not null
    Indexes:
       "dummy_pkey" PRIMARY KEY, btree (pk1, pk2)

See [SQLAlchemy
documentation](http://docs.sqlalchemy.org/en/latest/faq/ormconfiguration.html)
