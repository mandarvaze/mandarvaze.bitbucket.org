
Recently I started learning elixir. During this process I came across
two TIL blogs.

First one is [TIL app](https://til.hashrocket.com/) created by
Hashrocket.

At first they created it in
[Rails](https://github.com/hashrocket/hr-til) .

Recently they created (ported ?) it to Elixir.
[Source](https://github.com/hashrocket/tilex)

The other is more interesting to me. One of the (ex?) HashRocket
engineer Josh Branchaud has his own [til
repo](https://github.com/jbranchaud/til) Although he hasn't published
these anywhere (Other than this public repo) it contains a lot of gems.

The way these are different is that each entry is a tiny piece of useful
information. Small enough that I would not have called it a blog
"post". But who is to say how much should a blog post be ? After all,
several tweets that are just under 140 character contain the useful
information.

I too have collected several "notes" over the years. I never organized
nor publish them, thinking "no one needs them", but who am I to decide
? May be some one will find these useful.

So I have decided to publish these as and when I learn. I may post some of the
older ones initially as well.

I call them "Micro-TILs" !!
