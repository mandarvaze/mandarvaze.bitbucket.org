
<!--more-->

{{< youtube FtieBc3KptU >}}
Youtube Video of Jay Dixit's presentation.

Background
==========

I came across this video (I think) from Sacha Chua's weekly Emacs news
(But I am not certain, may be it was a tweet.) Personally, I prefer to
refer to the presentation (PPT/PDF) that goes with such video, assuming
it contains written notes that viewer can refer to later.

But I could not find the notes/presentation that go along with this.
Since the video contains a lot of gems, I decided to make my own notes
(and in process help some other emacs-noobs like myself)

Please note that there are lot of "visuals", so it is useful to watch
the video. At least the second half hour is "demo" mode, so there
couldn't be any notes.

Comment your articles
=====================

The way programmers write code comments, which are meant for themselves
(or other developers) Similarly, writers also need to comment their
articles (Who knew) These are Notes-for-self, but do not show up in the
exported format. Trick is simple, start a line with a `#`, and the line
is considered as as comment. Check the source org file of this article
and see a comment right below this sentence.

Off course for multi-line comment, one can use well known
`#+BEGIN_COMMENT` and `#+END_COMMENT` blocks.

If you want to comment out an entire subtree, use `C-c ;`. Such subtree
is not exported.

workflowy meets scrivener
=========================

Jay opens up with how he liked both these products separately, but
wanted combination of these

Outline to the left, contents on the right controlled by keyboard as
much as possible without using mouse a lot

He showed two "tricks" (I couldn't hear them properly, but here is how I
was able to get such a functionality)

### Split the window

Split the window to the right (assuming you started with single window)
In spacemacs you can do this by `SPC w V`

Now you have the same buffer in two windows. You edit in one windows,
but the changes are visible in other window immediately.

That is not exactly what you (or Jay) want. He wanted outline in one
(left) window and contents in the right. So we ..

### Separate the buffers

Go the window on your left, and `clone-indirect-buffer-other-window` Now
you can see the outline in the left pane, and edit in the right pane.
The left pane can remain "folded". It doesn't update unless you add a
node.

*Continue to [Part 2]({{< relref "post/emacs-for-writers-2.md" >}})*
