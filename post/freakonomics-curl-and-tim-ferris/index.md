
May 14, 2015
------------

- [James Altucher Show (Tim Ferriss experiment)](http://www.jamesaltucher.com/2015/04/ferriss/)
   -  When division is shutdown, new management/owner will not
      take any decisions.
   -  If the decision results into "loss", new management will be
      blamed for shutting down the project/division.
   -  If the decision results into "profit", old management gets
      the credit
   -  So leave it in a limbo :(

------------------------------------------------------------------------

May 15, 2015
------------

- [How think like a Child (Freakonomics Radio)](http://freakonomics.com/2015/04/29/think-like-a-child-a-freakonomics-radio-rebroadcast/)
   -   Children spot "magic" easily
   -   They are always making theories
   -   Adults have a pattern to learning based on what we already
       know (This may be good or bad)
   -   "Play" means do something without thinking of "utility"
       (Difficult for adults)

- [Why we laugh - TED Talk](http://www.ted.com/talks/sophie_scott_why_we_laugh)
   -   Humans aren't the only mammals that laugh (Apes, Rats laugh
       too)
   -   We can easily detect Fake laugh Vs real laugh
   -   Children can't easily detect Fake laugh, we learn (that)
       till we are mid-30s early 40s
   -   As we grow, we need social context to laugh (less
       contagious)

------------------------------------------------------------------------

May 16, 2015
------------

- [17 years with curl](https://changelog.com/153/)
   - Name change :
     -  Initially httpget but then supported FTP protocol,
        so changed to urlget, but it supported POST as well,
        so changed to curl
   - Program structure change :
     -   Originally one program, then split into libcurl -
         which other programs can use.
     -   curl binary also uses the library.
     -   php was (one of the?) first third party program to
         use libcurl
   - Version controls used : CVS, git
     -   skipped using svn for the project but Daniel was a
         core contributor for svn
   - Hosting change :
     -   Originally on sourceforge, now on github
   - License changed :
     -   Originally GPL (default). Too restrictive, changed
         to MPL (not compatible w/ GPL?) Finally MIT - most
         liberal license
