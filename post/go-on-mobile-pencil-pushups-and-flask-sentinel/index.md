
April 22, 2015
--------------

- The changelog podcast episode [148](https://changelog.com/148/) : The state of Go in 2015
  -   Go on Mobile - Seems like cool stuff
      - Andrew talks about move from Google code to Github(He isn't too happy)
        -   Google code was going away (Internal projects had to move first)
        -   He explains why Gerrit is better than github "pull request" workflow
        -   Github doesn't support CLA
        -   When I worked on OpenStack for a short amount of time, I had signed
            CLA and used Gerrit. Openstack used git (as opposed to mercurial)
            but not github.
- Raymond Hettinger's Presentation in Pycon 2015 : Beyond PEP8
  -   He uses live code instead of slides, which is good but it
      means we must consume the 1 hour long video. Sometimes
      slidedecks are better (Not always)
  -   He ran out of time (Due to lunch break, at that)

{{< youtube wf-BqAjZb8M >}}

------------------------------------------------------------------------

April 23, 2015
--------------

- [GraphQL](https://changelog.com/149/)
- After being disappointed that users must consume 1+ hour youtube video,
  I looked and came across Raymond hettinger [slidedeck](https://speakerdeck.com/pyconslides/transforming-code-into-beautiful-idiomatic-python-by-raymond-hettinger-1) It is not same, but lot of ideas from 2015 presentations are already covered here.
  -   python3 has @ignored exception context manager
  -   slidedeck has reference implementation for python2

------------------------------------------------------------------------

April 24, 2015
--------------

- Ask Altucher podcast: Changing Your Lifestyle Can Change Your Life [Episode 239](http://askaltucher.stansberry.libsynpro.com/ep-239-changing-your-lifestyle-can-change-your-life)
  - pencil pushups"
  - eye exercises
  - ginger+lemon for better digestion
    -   This is known to all the Indians since childhood. My
        grandma who lived till the age past 90, used to use
        this whenever she had upset stomach
- Talk python to me [Episode 1](http://www.talkpythontome.com/episodes/show/1/eve-restful-apis-for-humans)
  - EVE framework
    -   I had read about it some time ago. At that time it
        was very restrictive in a sense that it supported
        only MongoDB. Now it supports other databases via
        SQLAlchemy
    - [Flask sentinel](https://github.com/nicolaiarocci/flask-sentinel)
      -   Token based access control for Flask application
      -   When I tried Token based access control for the
          Flask app earlier, it was not trivial. You can read
          about it [here](http://mandarvaze.github.io/2015/01/token-auth-with-flask-security.html)
          So I think this might be worth looking into.
