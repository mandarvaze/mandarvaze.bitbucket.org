
In the "Tim Feriss show" podcast, Episode
[#248](http://tim.blog/2017/06/25/reid-hoffman-2/) : Ten commandments
of Startup Success, there is a story about "Buying eve.com domain"

Turns out this domain was owned by 5 year old girl named Eve Rogers. How
does one negotiate with a 5 year old ?

Luckily (?) the negotiations were with her mom, and the price of the deal was
:

-   Equity in the company (exact details not shared)
-   (Observer) seat on the board of directors for the daughter (Eve
    Rogers)
-   Multiple trips to Disney Land per year
-   $50000

A funny little story worth sharing ..
