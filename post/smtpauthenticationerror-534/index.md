
One of the website suddenly stopped sending emails from the "Contact
Us" form

My fellow engineer tried to debug the problem, but could not.

so I stepped in.

The first thing I tried was to verify that the username/password was
correct, by logging into the gmail account via the browser.

It was.

So then I considered changing to TLS from SSL, but before I did that on
production setup, I decided to do all the trials locally.

I have a sample code that takes all the mail settings, and sends a test
email using python\'s SMTP library.

If it works, it eliminates the issues related to the "setup", which
includes things like port, SSL or TLS and off course username/password
combination.

My first attempt failed with the following error:

```shell
smtplib.SMTPAuthenticationError: (534, b'5.7.14 <https://accounts.google.com/signin/continue?sarp=1&scc=1&plt=AKgnsbvW\n5.7.14 lAhhjNCbEJP2Q5KgrvLRQlVrk8jRfKn4OfRMsdzU2DGweFba4uAvMh21nZQfU9dw5RI7Dc\n5.7.14 E7XdA9kgDEsy4cLigYxpDA_u7nkED1ljI4umnTIgygAtCziJoK3j1PJlaJPPkiXxQIqKPJ\n5.7.14 Qtz8q2piGCXlwcb999SWr4msRuYT_98tnjtZatjVolJvO4rAfI_sYWDKQP3TmrGEY_kwdF\n5.7.14 HsCkRe7iVGCExAMuCnR5GSvYiQtzc> Please log in via your web browser and\n5.7.14 then try again.\n5.7.14  Learn more at\n5.7.14  https://support.google.com/mail/answer/78754 h29sm3440144pfd.145 - gsmtp')
```

(If you are too lazy to scroll all the way to right to see the complete
error, it says
`Please log in via your web browser and then try again`)

Since I had already tried logging via the browser, I was confused. But I
tried it anyway.

May be I copy/pasted one less character.

But that was not the case. Browser login worked.

Stackoverflow to the rescue.

Turns out one needs to turn ON "allow less secure apps" option in
accounts settings.

I did (Just to see whether it will work.) and test email was sent
successfully.

And so did the email from the "Contact Us" form.
