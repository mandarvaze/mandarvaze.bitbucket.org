
Background
==========

In case you are not familiar with Rubber Duck approach to solving a
problem, it goes something like this :

> Sometimes, if one spends enough time analyzing the problem, one can find
> the solution on their own. But most of the times, we don't. We get
> stuck and "ask" someone. Off course, that "someone" is not as
> familiar with the problem as you are. So in order to help you, they ask
> you questions (so they can understand the problem first, before they can
> help you) But it is often seen that when answering "them", you come up
> with the solution your self.

So where does the "Rubber Duck" fit in ?

The point is, "they" didn't solve the problem, "you" did. By
explaining the details. So in this case "they" might as well have been
a "Rubber Duck". You can explain the problem to a rubber duck, and
will achieve the same effect. See this [Wikipedia
Entry](https://en.wikipedia.org/wiki/Rubber_duck_debugging) for more
details.

I have noticed several times, that **right after** I post a query on a
mailing list, I will get "unstuck" myself (and I've posted answer to
my own query - so that someone else might be helped in future)

So here is a recent example of how I came upon an answer by talking to a
(metaphorical) rubber duck.

Problem
=======

I was getting the following error on heroku

```shell
app[web.1]:   File "/app/.heroku/python/lib/python3.5/site-packages/flask_mail.py", line 566, in init_app
app[web.1]:     state = self.init_mail(app.config, app.debug, app.testing)
app[web.1]:   File "/app/.heroku/python/lib/python3.5/site-packages/flask_mail.py", line 552, in init_mail
app[web.1]:     int(config.get('MAIL_DEBUG', debug)),
app[web.1]: ValueError: invalid literal for int() with base 10: 'True'
```

Based on the code it refers to, the error seems correct in isolation.
But `flask-mail` documentation says (and the previous line
in the stack trace also agrees) that value of `app.debug`
is used for `MAIL_DEBUG`

I had set the `DEBUG` as follows :

```python
class HerokuConfig(Config):
    import os
    DEBUG = os.environ.get('DEBUG_MY_APP', False)
```

I've set it to `True` from heroku console

```shell
$ heroku config:get DEBUG_MY_APP -s
DEBUG_MY_APP=True
```

But it looks like this somehow gets converted to **string**
`'True'`, resulting into an error

```shell
$ python
Python 3.4.3 (default, Apr 27 2015, 19:08:17)
Type "help", "copyright", "credits" or "license" for more information.
>>> int(True)
1
>>> int('True')
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: invalid literal for int() with base 10: 'True'
>>>
```

So how do I prevent the DEBUG from turning into a string ? I think there
is something obvious that I am missing.

Solution
========

the root cause was that `os.environ.get()` always returns a
string. So the correct way to set `DEBUG` is as follows :

```python
class HerokuConfig(Config):
    import os
    DEBUG = os.environ.get('DEBUG_MY_APP', 'False') == 'True'
```

------------------------------------------------------------------------

*Flesch Kincaid grade level score : 2.50*

*Flesch Kincaid reading ease score : 92.08*
