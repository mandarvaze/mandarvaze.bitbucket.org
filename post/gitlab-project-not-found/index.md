
Every now and then I used to keep getting this error. (The project
existed.) This error showed up once in a while. My go to solution was to
reboot the machine (cause it used to work) But for once, that too did
not work, and it led me to finding the real cause /solution. I'm glad I
did.

I had noticed that I get this error when SourceTree asks for passphrase
for some other publickey. But it wasn't always the case.

As normal troubleshooting tips, I tried
`GIT_SSH_COMMAND="ssh -v"` which showed that the correct
key was in use. This did not surprise me because by
`~/.ssh/config` was correct (Otherwise I would ALWAYS have
problem accessing gitlab, which was not the case)

Whenever I rebooted and used any git command afresh, I used to see
message \"Identity Saved\" (after I entered the correct pass phrase). I
had a hunch that somehow my problem was somehow related to this. I still
don't know what is the problem, but I came across the solution.

Turns out way to fix this is remove the identity using
`ssh-add` command.

```shell
$ ssh-add --help
ssh-add: illegal option -- -
usage: ssh-add [options] [file ...]
Options:
  -l          List fingerprints of all identities.
  -E hash     Specify hash algorithm used for fingerprints.
  -L          List public key parameters of all identities.
  -k          Load only keys and not certificates.
  -c          Require confirmation to sign using identities
  -t life     Set lifetime (in seconds) when adding identities.
  -d          Delete identity.
  -D          Delete all identities.
  -x          Lock agent.
  -X          Unlock agent.
  -s pkcs11   Add keys from PKCS#11 provider.
  -e pkcs11   Remove keys provided by PKCS#11 provider.
  -A          Add all identities stored in your keychain.
  -K          Store passphrases in your keychain.
              With -d, remove passphrases from your keychain.
```

I still need to investigate how to remove a specific identity (I didn't
spend much time. I just have two entries saved. I removed them both. Not
a bug deal)

Once I removed the saved identities, git command asked for the pass
phrase, and gitlab was accessible again.

------------------------------------------------------------------------

*Flesch-Kincaid grade level score: 2.40*

*Flesch-Kincaid reading ease score: 91.67*
