
Initially, I was going to title this either "How to improve your
writing", or "How to write better". But both those are subjective.
The current title is apter[^1] , I think. In technical terms, these are
errors/warnings in the prose. Errors you **must** fix. Warnings you
*should* fix, but can choose to ignore.

Since the "prose" isn't compiled, there is no compiler. This is
similar to various interpreted (programming) languages, where there are
no compilers, hence there is no easy way to catch the errors/warnings
before hand. In comes the "linters" - they "can" check for potential
errors before hand, and it is always a good practice to run the code via
the "linter".

The prose is the same. In fact, one of the tools is aptly named exactly
that `proselint`

So there are some of the tools I have come across recently.

[Grammarly](http://www.grammarly.com)
---------

As the name suggests, it is mainly used for checking the Grammar. Useful
for someone like myself for whom English is not the first language.

All of my writing is in emacs. So Grammarly does not directly integrate
with my workflow. There is a native app, where I can copy paste this
text and make corrections, and import the fixes "back" to emacs :) Bit
tedious if you ask me.

[write-good-mode]({{< relref "post/writegood.md" >}})
---------------

I've already written about it earlier. It is a minor mode in emacs. It
identifies weasel words (words that do not add value or change the
meaning), passive voice, duplicate words.

proselint
---------

This is a python program. you install it using `pip install proselint`

Apparently, there is a way to integrate this with emacs as well (Need to
research)

Here is an output from `proselint` when run on one of my
previous post[^2]

```shell
$ proselint ../posts/gitlab-project-not-found.rst
../posts/gitlab-project-not-found.rst:8:55: cliches.write_good 'Needless to say' is a cliché.
../posts/gitlab-project-not-found.rst:33:36: typography.symbols.ellipsis '...' is an approximation, use the ellipsis symbol '…'.
../posts/gitlab-project-not-found.rst:55:29: hyperbolic.misc '!!!' is hyperbolic.
../posts/gitlab-project-not-found.rst:55:30: leonard.exclamation.multiple Stop yelling. Keep your exclamation points under control.
```

When I ran it against some other posts, there was no output, indicating
that those posts were "fine" :)

------------------------------------------------------------------------

Footnotes:

[^1]: I had chosen to write "more apt". Grammarly suggested "apter".
    I had never heard of this word. I had to check to see whether this
    was a false report. It wasn't.

[^2]: I've since fixed 3 out of these 4 errors. The error about
    ellipsis is verbatim output from `ssh-add --help`
