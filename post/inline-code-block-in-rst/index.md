
TL;DR: use something like this: `` :code:`some text` ``

------------------------------------------------------------------------

I have been using `org-mode` for my blog posts lately. It provides all
the mark-up I need. Some days ago, I needed to cross-link to one of my
posts. `nikola` (the python based static site generator used to power
this blog) has a option just for that. (Refer to
[this](https://getnikola.com/path-handlers.html))

I tried to be smart, and used equivalent org notation, but it didn't
work.

So I converted that post to ReST. (Aside: ReST is lot more powerful
especially with reStructuredText extensions. But I may not need these,
most of the times, and `org mode` suffices.)

Now here is the problem.

I am used to org notation (inside of two equal-to signs) at times.
org-nikola plugin converts it to `<code>` HTML block, which I like. But
nikola itself converts equivalent ``` ``this`` ``` notation in ReST to
`pre`. HTML `<code>` blocks look better (due to syntax highlighting)
than `pre` blocks.

ReST has `:code:` notation, used for multi-line code block, but I
couldn't (till now) find a way to use `code` block inline.

But finally I found [this](http://stackoverflow.com/a/12365251/154947)

------------------------------------------------------------------------

*writing about ReST in orgmode is confusing. I ended up using ReST style
of* *links instead of org style*

*Flesch kincaid grade level score : 2.334*

*Flesch kincaid reading ease score : 93.79*
