
While listening to the Changelog Episode 221, where they interview Dr.
Cory Doctorow. In the Interview, Doctorow mentioned Ulysses Pact.

In the Greek mythology, it is said that whenever ship passes near the
Siren's island, the men would hear Siren's Song. upon which they would
lose their rational thought and jump the ship to swim towards the
Siren's island. (Sirens would kill the men)

Ulysses wanted to hear the Siren's Song, but he also knew the risks. So
he ordered his men to tie him to the mast of the ship so that he could
not jump the ship (when he will hear the Siren's song and go insane) He
also ordered his men to put wax in their ears so that none of them could
hear the song, and all of them would remain "sane" (and continue on
the course they had planned earlier)

This story drives the point that one can "protect oneself" from future
risks by planning for them now. To expand further on this, this
"advanced" decision is taken when the person is of "strong" mind and
recognizes that in future may become weak, and act in a way that may be
harmful.

e.g. When on a diet, instead of trying to "fight" against eating an
"Oreo" cookie (in a weak moment) It is better to "Not to buy the Oreo
at all" and/or "Throw away the ones you have" (when you are strong)

More recent example is that starting with iOS 8, even apple can not
unlock the user's iphone data (Only the user can) This means even if a
government body forces them to get the user's data, they (Apple)
can't.

------------------------------------------------------------------------

References :

-   [Changelog Episode 221](https://changelog.com/221/)
-   [How to Protect the Future
    Web](http://boingboing.net/2016/06/24/how-to-protect-the-future-web.html)
-   [Ulysses Pact](https://en.wikipedia.org/wiki/Ulysses_pact)
