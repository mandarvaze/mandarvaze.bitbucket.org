
I was trying to setup local postgres via docker, and wanted to see if my default
user was created. I found out that there is more than one way to skin the cat.

<!--more-->

On the `psql` prompt enter `\du` to list the users.

```
postgres=# \du
                                   List of roles
 Role name |                         Attributes                         | Member of
-----------+------------------------------------------------------------+-----------
 postgres  | Superuser, Create role, Create DB, Replication, Bypass RLS | {}
```

Adding `+` at the end prints the description (if any)

```
postgres=# \du+
                                              List of roles
     Role name     |                         Attributes                         | Member of | Description
-------------------+------------------------------------------------------------+-----------+-------------
 postgres          | Superuser, Create role, Create DB, Replication, Bypass RLS | {}        |
```

Third alternative is `\dgS+`

```
postgres=# \dgS+
                                                                            List of roles
         Role name         |                         Attributes                         |                          Member of                           | Description
---------------------------+------------------------------------------------------------+--------------------------------------------------------------+-------------
 pg_execute_server_program | Cannot login                                               | {}                                                           |
 pg_monitor                | Cannot login                                               | {pg_read_all_settings,pg_read_all_stats,pg_stat_scan_tables} |
 pg_read_all_settings      | Cannot login                                               | {}                                                           |
 pg_read_all_stats         | Cannot login                                               | {}                                                           |
 pg_read_server_files      | Cannot login                                               | {}                                                           |
 pg_signal_backend         | Cannot login                                               | {}                                                           |
 pg_stat_scan_tables       | Cannot login                                               | {}                                                           |
 pg_write_server_files     | Cannot login                                               | {}                                                           |
 postgres                  | Superuser, Create role, Create DB, Replication, Bypass RLS | {}                                                           |
```

Bit of an explanation of the above command(s)

* `\du` : Stands for "Describe Users"
* `\du+` : Same as above, but `+` signifies "list additional information"
  * `\dg` and `\dg+` also work, since "Users" and "Groups" are now mixed into "Roles"
* `\dgS` (`+` optional) : While other command shows user created roles, adding
  `S` shows system roles as well.
  
