I am a regular "listener" (?) of Freakonomics and "James Altucher
Show" podcasts among others. So when Steve Dubner and James Altucher
started a podcast *together* there was no reason to not subscribe.
Needless to say, their new show "QOD : Question of the Day" is both
entertaining and full of information.

This post is related to their episode no. 118 about deliberate practice. You can
listen to the episode
[here](https://soundcloud.com/question-of-the-day/118-how-can-deliberate-practice-help-adults-learn-new-skills).

Better still, why not subscribe to the podcast ?

Couple of interesting stories from this podcast ..

London Taxi Drivers Vs London Bus Drivers
=========================================

Both taxi drivers and bus drivers, drive vehicles (for other people)
full time, so in a sense their job is alike. It was expected that after
30 years of career in their respective jobs, the brain development for
both would be similar. Turns out, brain of the taxi driver evolved
"better" than that of the bus driver. Dubner goes on to explain why.

Taxi driver has to learn new route for each new passenger they carry (at
least first few years anyway) Bus drivers on the other hand have
"fixed" route, even if they have rotation every few weeks. So taxi
driver has to constantly practice, while bus driver doesn\'t. Hence the
difference between the growth of their brain.

General Practitioner
====================

Dubner comments that after years of "General Practice", brain of a
doctor remains at same level as the time they graduate.

When they are students, they are learning a LOT. Both via books and
experience. But once they start the practice, it is not up to them to
"choose" type patients they treat.

Doctor "dignoses" the patient, prescribes the medicine, and that is
the end of it. Patient may not return for various reasons. May be the
medicine "worked", may be they went to another doctor, may be patient
died !! (Yes, they say that on the podcast) May be the patient would
have been cured on their own.

There is no way to tell (unless patient returns for the follow up)

James says that the feedback is not enough (in the above case)

Mentor
======

They talk about how "quick enough" feedback is important. Sometime
(like in formal education) feedback is easy in the form of test scores.
But in some cases it is difficult. e.g. "writing", or other creative
art. You may not even *get* any feedback EVER. or not enough, or not on
time. Also the feedback might be subjective and may not be directly
useful. "I liked (or did not like) your painting" - How do you use
such feedback ?

Mentor can be helpful in such cases.

Aside
=====

Not covered in the podcast, but the subject of "Deliberate practice"
also came up in the coursera course "Learning how to learn". They say
that attempting varied and/or (perceived) difficult problems can
"solidify" your understanding. You not only understand "how", but
also "when" to use the particular technique.
