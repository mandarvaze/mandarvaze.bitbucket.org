I have used many editors (chiefly `vim`) and IDEs (pycharm) My
colleagues use *sublime text* so I gave it a try, but always switched
back to either pycharm for `python` coding, and `vim` for everything
else (until I came across `spacemacs`, and now that is my main editor
for last few months. I still use `vim` occasionally, on remote server
etc.)

Back to sublime text ..

One of the thing I liked about sublime text was that I could just open a
new window, start typing and even after I quit/restart sublime, my
contents would be there.

So even log after I stopped using sublime for coding, I continued using
it mainly for this purpose alone.

I have several such "windows" from few months ago, and I found what I
wanted there (Since I started using space/emacs, I have started using
`org-capture` for such things, but I still find myself using sublime for
creating persistent scratch buffers)

It never occurred to me to look for similar functionality in emacs
(kinda highlights my noob status, since an experienced emacs user knows
that nothing is impossible with emacs) and then I came across
[this](https://www.reddit.com/r/emacs/comments/4cmfwp/scratch_buffer_hacks_to_increase_its_utility/)
reddit thread.

persistent-scratch
==================

Installing it is a simple matter of adding it to
`dotspacemacs-additional-packages`, and then `SPC f e R`

At first, I got `file-error` since package list was stale. Restarting
spacemacs fixed that (turns out I could have easily done
`M-x package-refresh-contents`)

Then I had to add `(persistent-scratch-setup-default)` to my
`dotspacemacs/user-config`, and that's it.

I now have scratch buffer that will retain its contents across restarts.

parting thoughts
================

I still think this is not same as what sublime text does.

The reddit thread also mentions `open-junk-file` (which is exactly my
use case, since I don't care about the filename or its location, just
the contents) but `open-junk-file` creates files using timestamp.

Plus, using single scratch buffer is not same as ability to use as many
scratch buffers as needed. I guess I can always have one giant
`*scratch*` buffer, divided using `org-mode` headings

But then why not simply use `org-capture` ? That is what it is meant
for, isn't it ?

Oh, well ...

----------------

Update : Feb 27, 2018

For several months now, I am using TextWrangler on macOS, which has similar
feature that retains the contents of "new" buffer across restarts without having
to explicitly save the contents.
