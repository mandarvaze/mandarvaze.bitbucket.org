
Why is this interesting to me ?
===============================

Since LastPass was acquired. I switched to Dashlane, which seems good,
but there is this nagging feeling that Dashlane might stop the free
version, or something.

I had used KeePass long time ago. But the Problem is "syncing" KeePass
Web solves (in theory) this problem by supporting Dropbox syncing "out
of the box". All I really need is backup.

If you are interested, you can download KeeWeb
[here](https://github.com/antelle/keeweb)

Stumbling block
===============

I installed the desktop app, which is based on electron shell. (Other
apps using electron are Atom editor, and Visual Studio Code)

On OSX, I was not allowed to open the app, since the app is not signed.
This is clearly mentioned on the site, that the author doesn't plan to
spend money on this (yet)

But do not despair.

OSX allowed to open this via the following method:

1.  Find the app in the finder
2.  Ctrl-Click to open
3.  You see a message, click OK
4.  Now onwards the app will open without error

No-Deal
=======

Finally, I didn't switch, mainly because importing the existing
database was not easy. Creator of KeeWeb mentioned that he may support
import in [later
release](https://twitter.com/kee_web/status/688966742870257664). He
suggested using native KeePass to do the **One time** import This kinda
defeats the purpose, but I was OK to try it, since it is only **one
time**.

Unfortunately, installing native KeePass on OSX is [not
trivial](https://twitter.com/kee_web/status/689168873183178755).

KeePass is essentially a "Windows" program. Supported on OSX and Linux
via `mono`.

I could have tried this route, if I were planning to use KeePass native
as a long term/permanent solution. Since that is not the case, it is too
much an effort, hence decided against it.
