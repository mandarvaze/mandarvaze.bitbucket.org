
I started using Heroku for one of the projects recently. In past, I have
used Openshift, Webfaction (and AWS long time ago). Each of these gave
some kind of `ssh` access. By ssh access, I mean access to
the *running* system.

**Heroku is different.**

Heroku gives you `bash` access. At first, it may seem
similar to the ssh access, but it is not. Each time you run
`heroku run bash --app my_app` Heroku creates a new
instance of the shell. The filesystem is ephemeral i.e. if you have two
such `bash` shells are running, and if you write something
to `/tmp` from one instance, it won't be visible to the
other `bash` instance.

Since there is no "instance" running (at least in a traditional sense)
one can't send or receive files from the application "instance"

The only way to "send" files to application instance is to have those
files committed to `git` and push them to Heroku. After
all, that is how you update the code on Heroku.

Now, that's a problem.

Create Files
============

One might ask "why" would I want to send files to Heroku which are not
in `git`

Here is an example:

I have scripts that create some artifacts by reading the CSV file as an
input. I may need to run these scripts from time to time with different
CSV files each time. The input files are temporary in nature. They may
also contain sensitive information which I do not want to commit to
`git`

Normally, I would `ftp` the files to `/tmp` of
the machine, `ssh` to the instance and run the script by
pointing it to `/tmp/myfile.csv`

As explained earlier, this does not work for Heroku

So I created a support ticket with Heroku, and here is an option they
suggested.

First "login" to Heroku via
`heroku run bash --app my_app`

```shell
$ mkdir CSV_files
$ cd CSV_files
$ cat <<'EOF' > myfile.csv
first_name, last_name
John, Doe
Jane, Doe
Mickey, Mouse
Minnie Mouse
EOF
$ cd
$ python scripts/script1.py -i CSV_files/myfile.csv
```

This is not the most optimal way.

I run `cat myfile.csv` on my local machine, and then
copy/paste to the Heroku terminal session, after the `cat`
line

Finally, I type in `EOF` manually, to finish the input.

This is not the most optimal solution, e.g. for *long* files, this could
be a problem.

While there could be side effects due to how terminals handle
"copy/paste", this seems to be the only option if one doesn't want
the files in `git`

Save Files
==========

Now that I am able to run the script on Heroku, I need to save the
output from the script. Most of the times, this is not a problem. I use
the "copy/paste from the terminal" But sometimes, the output is so
long that it scrolls past and does not fit the terminal window's
buffer.

Normally, I would redirect the output to a file (in /tmp) and FTP off
the server, but again, it doesn't work with Heroku.

I came across this [post](http://stackoverflow.com/a/40721844/154947) on
SO.

So there is this service that would accept files from anyone, anywhere,
called [transfer.sh](https://transfer.sh/). You can check the details on
their site.

I was a bit hesitant letting someone-I-don't-know, host my files, but
it was only the log output, that did not contain any sensitive data.

If you have any sensitive data, you can encrypt it, before uploading

You can also control the duration for which your files are accessible.
The default is 14 days.

I understand that neither of these solutions is optimal, but they work
in a pinch.

------------------------------------------------------------------------

*Flesch-Kincaid reading ease score: 93.73*

*Flesch-Kincaid grade level score: 2.98*
