
Refer to Part 1 [here]({{< relref "post/emacs-for-writers-1.md" >}})

------------------------------------------------------------------------

Here are various other tricks Jay talks about, in his presentation.

org-bullets
-----------

Makes pretty bullets in `org-mode`. `spacemacs` already comes with this
package. No additional work needed. Off course, you can customize the
bullets to your liking, but I am pretty happy with the defaults.

notmuch for emails
------------------

Jay doesn't show `notmuch` but makes a comment about it. I looked at
it, but I think my use case is bit different. I would rather have
something like `mutt` , than something just *searches* and outsources
sending and receiving to other programs or libraries.

google search
-------------

I was amazed to see that emacs lets you search google from within
(without opening up the browser) `spacemacs` lets you initiate a search
(but opens the results in a browser) Try `SPC s w g`

Abbreviations
-------------

This is like a autohotkey program some of you might have used. As a
writer, Jay uses it extensively. He shows his `abbrevs_defs` file, which
is huge. Jay used abbreviation bq to insert `#+BEGIN_QUOTE` and
`+END_QUOTE` pair. may be it was a snippet (`yasnippet` ??) I couldn't
make out.

nvALT
-----

Jay says that if someone wants to move away from MS Word (he is very
unhappy with MS Word as a "writer's tool" (Hence the need to combine
workflowy with scrivener, refer to previous Part 1) he would recommend
this program to them rather than uphill learning curve of Emacs. I
looked at it, but the program doesn't seem to be updated in at least
couple of years. Plus there is a `deft` mode in emacs, which I think is
similar

fountain mode
-------------

Jay shows a screen play. Apparently `fountain-mode` is a standard
(text-based) file format, understood by others tools as well.

poetry mode
-----------

counts number of syllables in the line, shows words rhyme. with the word
under cursor I was blown away to see the demo.

org-mac link grabber
--------------------

I couldn't catch the details. This essentially does a lot of things
like capture the URL from the browser, and inserts it into an org-mode
file, with the text and URL. This is useful for references.

*[Update: 2016-06-21 :
[Here](http://orgmode.org/worg/org-contrib/org-mac-link.html) you go]*

Which emacs (distribution) do you use ?
---------------------------------------

Jay settled on railwaycat emacs after trying `aquamacs` It has a lot of
`OSX` specific functionality like :

-   pinch to zoom
-   swipe to navigate between frames

since `spacemacs` documentation already
[suggests](https://github.com/syl20bnr/spacemacs#os-x) this
distribution, turns out I was already using it but didn't know the cool
features.

Setting the title of the window
-------------------------------

Out of the box, my Emacs window has a boring title like `Emacs` instead
of the filename (Turns out I had not noticed that.) Seems like there is
a variable for "fix" that.

I couldn't catch the details from the video, so I
[asked](https://twitter.com/mandarvaze/status/694253808138891264) him about it,
and he was kind enough to
[respond](https://twitter.com/jaydixit/status/696394667869544448).

buffer stack
------------

Shows the demo of how to move thru the relevant buffers/file. He has
configured it such that buffers like `*Messages*` are not part of the
stack

config files in org-mode
------------------------

So that they can be well documented/shared. This can be done via
`org-babel`, although being a beginner, I am yet to do that myself.

Closing remarks
---------------

I want to Thank Jay for this presentation, and
[thoughtbot](https://www.youtube.com/user/ThoughtbotVideo) for sharing
this video with us.

It is inspiring to see a "non programmer" use emacs so "ably". As a
non-programmer, he brings in a unique point of view.

It is also comforting (for a emacs beginner like myself) that if a
non-programmer can make so many customizations, solely depending on the
kindness of the emacs community at large, then deciding to learn emacs
after being a `vim` user for 20 years ain't bad choice.
