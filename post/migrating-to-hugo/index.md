
## Background

When I started my second blog, instead of pelican, I tried awesome Nikola SSG.
It is truly awesome. What I (initially) liked about it was it had support for
`org-mode` i.e. I could write my blog in `org-mode` and Nikola would generate
the post out of it.

I spent looking for "decent" theme, but couldn't find one. I tried material
theme for a while, but realized it could not do syntax highlighting.

Since many of my posts had some code, it was an important requirement. But in
order to not get "stuck", I chose a one that I could "live with", but I was
never happy.

When I heard about Hugo, I was impressed by the vast number of themes. I wanted
to "move" immediately.

But by then, I was "locked" into Nikola due to it's built-in Gallery feature.
One just drops files into a folder, and that is all. (Hugo requires me to list
the filename, which may be good, but I wonder how well it would scale.)

I could not get Gallery to work "back then". You can read about my first attempt
[here]({{< relref "post/getting-started-with-go.md" >}})

## Process

When I started again, I started playing with the `exampleSite` that came with
the theme. I tried to get Gallery feature to work. (Was quite easy. Theme
documentation has an example)

Once prototype worked, I had to convert my posts from `org-mode` and `rst` into
`markdown`

### Initial conversion to markdown

I know hugo supports `rst` (and `org-mode`), but I am tired of having to "tweak".
(At one point, it was fun. Now I just want to focus on writing. This is also the
reason I moved from linux to macOS few years ago.)

So back to "conversion" I used the following to get the "initial" conversion.
"Initial" you say ? Indeed it is just the beginning. Read on.

```shell
$ for f in `ls -1 *.rst`; do
n=$(echo $f | cut -d'.' -f1)
pandoc -f rst -t markdown -o /hugo/basedir/blogname/content/post/$n.md $f
done

$ for f in `ls -1 *.org`; do
n=$(echo $f | cut -d'.' -f1)
pandoc -f rst -t markdown -o /hugo/basedir/blogname/content/post/$n.md $f
done

```

### Fix front matter

While the "promise" of SSGs is that if you write the content on the
`$common_format` you can easily switch, it reality, it is not easy, as I found
out "second" time.

When I had two SSGs, pelican and nikola - they both had "slightly" different
"style" (for the lack of better word). To their credit, nikola eventually
supported front matter that works well with pelican. In fact, I was using same
post (mostly) as-is between pelican and nikola for some time. (Mostly because
things like "link to other posts" etc. are different.)

Hugo on the other hand "quite" different style.

* Note: Hugo allows three formats for the front matter. TOML, YAML and JSON.
Initially I tried TOML, but syntax is too different `=` as opposed to already
used `:` So for later "coversions" I went with YAML *

Mostly the "values" in the front matter are double quoted
strings, which is not the case for nikola. This was easy in `evil` via
`ysiw"`

### Change `category` to `categories`

Hugo is probably first SSG I have come across that supports multiple categories.
Others usually go with "one category, multiple tags"

So aside from changing the key name, I also had to enclose both categories and
tags into square brackets like `[]` and each value to be "double quoted".

Then there are other smaller things like :

* Fix dates: I had full datetime stamp
* Remove `{.sourceCode}`. This was auto inserted by `pandoc` conversion for `:code:` in the rst files.
(Refer to [Inline code blocks]({{< relref "inline-code-in-rst.md" >}}) post,
about how I used it earlier.)
* Replace `\"` and `\'` with normal double and single quotes. These too were inserted by `pandoc`

### To automated or not ?

I did consider writing script or something to automate this, but didn't. I did
most of this manually. Off course, knowing vi commands did make my life easier.
(But one could have done this with any other editor. Most of them have
"find-and-replace".) On a related note, I came across
[this](https://thekennethlove.com/2018/02/real-world-programming-or-the-whole-damn-point-is-to-solve-problems/)
post.

## Migrating galleries

Instead of dumping everything under banner, I created subfolders under
`static/images`

I copied the files from my old blog structure to this new structure.

One of the benefits of nikola was that I could just dump new files, and
automatically they would show up. Here I need to explicitly list each and every
file.

The down side was the order in which they would show up was very random (Probably
due to internal use of dictionaries - where the order is never the same. But
this is just a guess.)

So I decided to use this opportunity to display the images in reverse
chronological order (in simple terms - latest image shows up first. Makes sense
ain't it ? Regular visitors may have already seen the old images, and may be
interested in the new content.)

While this works well going forward, populating this list "by hand" was a tough task.

I used `ls -lrt1` to get the list of files from newer->older.

I copy pasted the results to the `.md` file. (Removed `..` and other files)

Then using vi/evil magic - added the fixed path to all the entries.

## Git stuff

After spending a couple of days working on the changes in the pockets. I was
ready to replace my site.

I couldn't just do another `push` since some of the folder names might be same,
but since contents are different, I didn't wanna take a risk.

So I wanted a clean start. (But I also wanted to retain the history)

I didn't know how to do that.

One option I considered was doing `git rm *` and commit this with a message
"Reboot"

I could have always gone back using `git log` history, but that would be
cumbersome.

So I decided to move the existing site into it's own branch, so that if
required, I could just switch the branches.

I first renamed existing `master` to `nikola` - just to be safe. `git branch -m
master nikola`

Before I **delete** the master, I needed to tell bitbucket that `master` is no
longer the main branch.

So from bitbucket website, I chose `nikola` to be my main branch.

Then deleted the `master` using `git branch -D master; git push origin :master`

Now the `master` was empty (didn't exist).

A simple `git add .; git commit; git push origin master` created `master` in the
bitbucket.

But ..

Repeatedly hitting "Cmd+R" kept on showing my old site. My heart sank ..

Then just as a trial, I went back to bitbucket and made `master` the main branch
again.

and that was it.

New site was ready.

## Final thoughts

This is my third SSG. I started with pelican. Then nikola. and Now hugo.

While all of them support `markdown`, I personally prefer reStructuredText, I
think it is much more expressive.

It does not help that `markdown-mode` in spacemacs is broken. Specifically
`evil` IMO, may be more. So editing becomes more tedious.

When migrating, I used to switch to `text-mode` to get away from the "broken-ness"

Then there is also the "special" syntax specific to each SSG called "short
codes". Since there is no common standard for it, each has to invent their own.
They are similar yet not **exactly same**. So while porting, it indeed becomes
"additional work".

I really hope I stick with this for a (long) while, but "tinkerer" in me, is
gonna want me to look for better theme at least. But it will be a while before
that.

I do want to move my [other site](https://mandarvaze.github.io/) to hugo. May be
a different theme. Or I'll just have a single blog like I had
[planned](https://mandarvaze.github.io/2016/08/easier-blogging.html#finalize-the-blogging-platform)
