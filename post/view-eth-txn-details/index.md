
When working on one of my projects, it was asked "How do we ensure that
transaction was indeed recorded on the blockchain"

While we can (and did) store the transaction hash returned by the function call
in the database, `txn_hash` alone does no good.

e.g. "Official" method to get the details of the transaction (from the
`txn_hash`) returns something like the following:

```
web3.eth.getTransaction('0x9fc76417374aa880d4449a1f7f31ec597f00b1f6f3dd2d66f4c9c6c445836d8b§234')
.then(console.log);

> {
    "hash": "0x9fc76417374aa880d4449a1f7f31ec597f00b1f6f3dd2d66f4c9c6c445836d8b",
    "nonce": 2,
    "blockHash": "0xef95f2f1ed3ca60b048b4bf67cde2195961e0bba6f70bcbea9a2c4e133e34b46",
    "blockNumber": 3,
    "transactionIndex": 0,
    "from": "0xa94f5374fce5edbc8e2a8697c15331677e6ebf0b",
    "to": "0x6295ee1b4f6dd65047762f924ecd367c17eabf8f",
    "value": '123450000000000000',
    "gas": 314159,
    "gasPrice": '2000000000000',
    "input": "0x57cb2fc4"
}
```

It doesn't really mean much - even to the developers. Sure `from` and `to`
addresses provide some useful data, but we don't know what "operation" was
performed and with what data. (The data is in the `input` field shown above, but
does not make sense "as-is")

What if you could see something like the following, instead ?

```
{ name: 'mintToken',
  types: [ 'address', 'uint256' ],
  inputs: [ '5bb4b21e60d0033a1b86b83e4a1f8307ab2d01f9', <BN: 64> ] }
```

You get the function **name**, not an address, and the parameters passed to the
function - which can be "somewhat" useful, depending on the the types of
parameters. 

e.g. in the above sample, the first input param is the address and the second
one  - a number.

It may be easy to guess (from the function name) that we are minting 0x64 i.e.
100 tokens for a given address.

Isn't that much better ?

Getting such "human readable" output is made possible by
`ethereum-input-data-decoder` library - which does the *heavy lifting*

I added some wrapper functionality to make it as generic as possible without
having to make code changes.

You can pass the `txn_hash` and the path to the JSON file created by `truffle
compile` as arguments to the script.

Thus it works for **any** contract.

-----

You can see the source code
[here](https://github.com/mandarvaze/eth-playground/tree/master/utils)

If you have any queries (about the code) feel free to file a github issue (or
ask here in comments)
