
I came across this parable in the book, I am currently reading. It is a
Marathi translation of Biography of J. P. Vaswani "Guru of None,
Disciple of All"

Parable
=======

In one of the incarnation, Buddha was a prince, who did not speak.
Everyone assumed he "could" not speak.

Years passed.

Once the prince was traveling thru the forest. He heard two birds
"talk". A hunter heard the sound, and immediately killed one of the
birds.

The prince asked the bird "Why did you speak ?"

Hunter told everyone that he heard the prince talk.

Already sad King became angry, and decided to "hang" the hunter to his
death.

Prince asked the Hunter "Why did you speak ?"

Everyone heard him, and the king released the hunter.

Moral
=====

If the bird had not spoken, he would be safe. and the same goes for the
hunter too.

Dada Vaswani goes on to explain that we "talk a lot". We should divert
the energy (that we waste) inside.

------------------------------------------------------------------------

*edit: 2016-03-10 Thu 01:01*

*After using `writegood mode`, converted passive voice to active voice.*
