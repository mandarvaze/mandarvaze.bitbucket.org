
Beethoven's music has three distinct phases - early, mid and late. This
is very uncommon, since most musicians have a "set" style through out
their career. Fifth symphony was written during the "mid" part of his
career.

Beethoven was working on the fifth for four years.

Beethoven had started going deaf around this time, that may be the
reason parts of the symphony are "loud"

Four letter motif of the Beethoven's Fifth symphony was used by BBC
during the second world war because :

-   It sounds like morse code letter "V"
-   Roman numeral for five is "V"
-   "V" for victory

Reference : Classical classroom podcast [Episode
72](https://www.houstonpublicmedia.org/articles/arts-culture/2014/12/29/55020/classical-classroom-episode-72-you-dont-know-fifth-with-emily-reese/)
