
If you install python via the package manager for your OS
(`brew`, `apt-get`, `yum`) you
can have only one version of python at the same time.

At best one python2 and one python3

But if you want to have (and want ability to easily switch between) say
3.5.2 and 3.6.0 (and 2.7.2) then you should definitely consider using
`pyenv`

`pyenv` is not platform specific. It installs all the
versions in your home directory under `~/.pyenv`

Some useful commands:

```shell
pyenv versions  # Lists the various versions on your system
pyenv install -l # Lists available versions you can install
pyenv install 2.7.0 # Install Python 2.7.0
```

Off course there is lot more [on the pyenv
page](https://github.com/pyenv/pyenv)

There is a `pyenv` plugin that helps manager virual
environments. See
[here](https://github.com/pyenv/pyenv-virtualenv/blob/master/README.md)
