
Recently I started learning blockchain and related technologies.

There is SO MUCH to learn.

I started with blockchain basics (and it inevitably uses bitcoin as an
example)

If you are new to blockchain (not ZERO knowledge) I recommend you
checkout the [blockchain demo](https://anders.com/blockchain/)

It is one thing to **say** you can not modify "old" blocks, but
visualizing it really makes the difference. (It is important to
understand it for the topic under discussion)

Anyway, back to "uncles".

If you know how bitcoin mining works, you know that "orphan" blocks
are discarded. (i.e. yours was not the longest chain)

They also lead to "wasted" efforts, since miner does not get any
reward for such blocks.

But ethereum is different.

In ethereum, such blocks are called "uncles", and it "encourages"
the miners to include the uncle blocks. (and yes, miners are rewarded
for uncles)

The only reason (that I understood) is that including these blocks makes
the chain "heavy", and thus more secure.

(If you haven\'t understood *why/how* it becomes secure, please watch
the blockchain demo *again*. See the link above.)

------------------------------------------------------------------------

On a lighter note, but still relevant.

Someone on reddit explained these blocks as :

They are same level as your "Dad" (parent block)

They were "almost" going to be your "Dad", but didn\'t. Your "Dad"
beat them. (i.e. Your parent block was the "longest chain", and thus
"won".)
