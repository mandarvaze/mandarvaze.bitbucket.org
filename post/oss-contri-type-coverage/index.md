
Recently I did a couple of open source contributions. I decided to document my
learning here.

In this pull request I added Check for type coverage to travis CI for
`pyramid_heroku` project.

<!--more-->

## Project

The project explains itself as :

> A bunch of helpers for successfully running Pyramid on Heroku

## The Change

The code base already uses python type annotations, but it is not consistently
applied thru out the project. The issue was created to ensure that every time CI
runs, it checks for the type coverage, and fails if the coverage is below a
certain threshold.

* I "borrowed" the `type_coverage` script for another project.
* I added couple of commands to the `.travis.yml` after successfully testing the
commands locally.
* I also needed to make some minor changes to the code for `mypy` to even finish.

## What I learnt

This was the first time I "used" travis. `.travis.yml` seems easy.
I had considered running travis locally, but on discussion, it was deemed
"unnecessary".

*May be I should set it up locally sometime, just to understand the details.*

I had only used "python 3 styled" type annotations in my past project,
I learnt about : type annotations in the comments.

While the change itself was trivial, I learnt that the build fails on every
"non-zero" exit value, for each command. This meant that **even though everything
seemed to working OK locally, my PR kept getting (auto) rejected, due to build failures**

Since ideal goal is to have 100% type coverage, I started with that, till I
realized that, that itself was making the build fail.

After discussion, I "lowered" the threshold to 15% (which was the existing
coverage)

## The Pull Request

For your reference, you can see the PR itself [here](https://github.com/niteoweb/pyramid_heroku/pull/28)
