
**This is not really something I learnt this week. Just something I felt
compelled to share**

-------
The following advise does not assume that your security will greatly improve.
But making these minor changes will have disproportionate improvement to the
security.

<!--more-->
## Great passwords

Are random string like `5v!kal@1B9z(gwp-`
Non guessable - but impossible to remember.

You *must* write them down - which adds to a security risk.

Hence the following "practical" advise.

## Bad passwords

Do not choose something like 

* firstname@123
* companyname@123

"12345" may be only marginally better than above.

## Slightly better passwords

If you *must* use your or name of the company you work for in the password - be
little creative. These are (in my opinion) still easy to remember but not
"so easy" to guess.

### Use different numbers 

* firstname@987
* company@135
* name@246

### Use different symbol

* name!234
* name$975

### Use capitalization

Put an upper case letter somewhere in between rather than at the beginning, as
your instinct might tell you to do.

* naMe#468
* nAmE-159

### Use longer passwords

Use more numbers and/or letters - may be your last name.

* fir124St578lAst
* comp*54321any

*It is better to use the combination of these ideas* as you can see in the
examplex above. They progressively get difficult to guess.

### Do not reuse the passwords

Even if you make these changes, but use the same password everywhere, then you
still have a problem.

If you have too many passwords (doesn't everyone these days?) - use a password
manager.

I use [KeePassXC](https://keepassxc.org/download/) But there are other good
options like Dashlane, 1Password, LastPass. Search 

-----

No post about password is complete without obligatory reference to this
wonderful XKCD comic. So here you go ...

{{< figure src="https://imgs.xkcd.com/comics/password_strength.png">}}
