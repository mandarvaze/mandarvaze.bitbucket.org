
*[This post was originally written in March 2016, but published only
later]*

------------------------------------------------------------------------

This is not a "How" to get started with Go post. This post chronicles
my attempts, and reasons to get started with go.

Why
===

I am fascinated with the hugo static site generator tool. I am in a
process to migrate one of my blogs to hugo (But it is too soon.)

While reading thru the hugo documentation, I came across hugo-gallery.
But unlike hugo itself, hugo-gallery is available only as source
code.[^1]

Since I have been considering (but putting off) installing go locally,
this seemed like a good excuse to do so.

Installing via brew
===================

Official document does not mention/recommend brew install, but brew is
such a nice tool, I almost do not want to install any mac software
without it.

Since I chose the path different from what is officially recommended,
there was an opportunity to "learn" I wanted to install
`go tour` locally, but it won\'t work. But I wasn\'t the
first or only one to have this problem. Also it is usually "shoot
first, read documentation" attitude that causes the problem.

All I had to do was set the `GOPATH` variable. (and `go get` the gotour.)

BTW, one more deviation from the documentation, the way to invoke is not
`go tool tour` as the official document recommends. (But
after so many years, I know that usually documents grow "out dated"
after a while.)

Anyway, I was able to run the go tour as `./gotour` after
that.

-------

Update : Feb 27, 2018

It is only after (almost) two years, that I finally moved my blog to `hugo`

[^1]: Update: hugo-gallery is now available as a binary release
    [here](https://github.com/icecreammatt/hugo-gallery/releases)
