
OK, it wasn't really **that** bad, I could have easily "lived"
with it.

Background
==========

While thinking of creating API documentation for my project, I first
considered Markdown syntax (Later I selected ReST, since sphinx supports
it better.)

I also wanted to keep the documentation in .org files. I was planning to
export these .org files to .md , which I can commit to git. .md other
users can edit, .org is very emacs specific. It is unfair to assume
others will be able to maintain them comfortably

Problem
=======

At first, I didn't get "export to markdown" option at all, under
`SPC-m-e` I looked around, and tinkered with my spacemacs configuration.
I am sure I did several trial and **errors** Finally something worked.

I wanted "export to simple markdown", all I got was export to GFM
(GitHub Flavoured Markdown) I didn't like the `<div>` tags in the
markdown

Now I didn't know how to "rollback" I commented/removed (what seemed
like) relevant config changes, but it didn't help

Solution (?)
============

I turned to Time machine, and restored .emacs.d from few days ago. The
restore erred out midway

Now when I started emacs, I got vanilla emacs Hence the need to "fresh
install"

I first renamed existing .emacs.d to `.emacs.d.broken` and existing
`.spacemacs` to `.spacemacs.broken`

Then `git clone` d it from spacemacs repo

After spacemacs was setup (Takes really long time for the `git clone` to
finish) I compared `.spacemacs` file with the old one, and ported my
changes over.

------------------------------------------------------------------------

### Notes:

1.  Turns out Sublime and Vim support Org mode too. See
    <https://twitter.com/mandarvaze/status/681171214103085057>
2.  This post written in `.org` format. Thanks to [Org-mode plugin for
    Nikola](http://www.wikemacs.org/wiki/Nikola)
3.  Somehow using footnotes syntax of Org mode didn't export well in
    this Theme.
