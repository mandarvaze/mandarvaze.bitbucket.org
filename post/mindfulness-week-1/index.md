
I started an online course called "Mindfullness for Wellbeing and Peak
Performance" at Monash University on
[FutureLearn](http://FutureLearn.com). (*Name of the course is a
mouthful, hence I decided to shorten the name as referenced in this
post*) These are my notes from Week 1

------------------------------------------------------------------------

Mindfulness means being engaged and present in the moment. We are
usually mindful during the activities that we enjoy. When we are not
happy is when our mind wanders off (to worry, thinking about the past or
the future) and so that is when we **need** to be mindful.

Notice that mind has wandered off, and bring it back to the present
moment.

When we are not mindful, we are in default mode, where the mind wanders,
mostly thinking about past or future, causing stress (and not being
productive)

**Attention Deficit Trait :** When we multi-task, we are working on one
thing and thinking on five other things. Or getting distracted by the
phone call, or knock on the door. It activates amygdala so much that
we\'re getting to a sort of a chronic fight or flight response

Multitasking is an illusion
---------------------------

**Attentional Blink :** quick pause when we switch the context. During
this short 0.2=0.5 seconds, we are not paying attention to anything at
all.

To practice mindfulness, we need to manage the environment as well (Turn
off all the alerts on the computer and/or phone)

**Stress performance curve :** Zero stress may mean we are apathetic,
little bit of stress is good, motivates us (Top of the bell curve) and
more stress, then reduces the performance.

Mindfulness helps reduce the stress, but that doesn\'t mean it impacts
performance (based on the stress performance curve) rather when we are
**really** focused, we are "in the zone" (or flow state) and have
highest performance.

**allostatic load :** when we're activating the stress response when we
don't need it, it produces a wear and tear on our system that's called
allostatic load. This causes immune problem Cardiovascular problems etc.
mindfulness is that it helps to switch off the inappropriate activation
of that stress response and takes off a lot of that allostatic load.

stress actually accelerates the aging on the level of the DNA and
mindfulness has even been found to actually start to improve genetic
repair and slow down that aging process.

**Mindfulness Journal :** Write about mindful experiences and unmindful
ones too. The aim of this is learning, not criticising ourselves for not
getting it right. Mindfulness is about being curious and accepting,
rather than judgemental, and your journal should reflect this approach.
