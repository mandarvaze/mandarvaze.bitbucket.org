
Recently I did a couple of open source contributions. I decided to document my
learning here.

In this pull request I fixed a
[bug](https://github.com/pre-commit/pre-commit/issues/1007) for
`pre-commit` project.

<!--more-->

## Project

The project explains itself as :

> A framework for managing and maintaining multi-language pre-commit hooks.

I was aware of the concept of pre-commit hook in git for a while. But I always
considered it as something to the done on git "server". I did not know that I can
(and should) install it on client side, *and* that it is so easy to "install"
the hook. (It is as simple as `pre-commit install` once the package itself is
installed via `pip install pre-commit`)

## The Change

The person reporting the bug had already pointed to a possible solution,
including the file and function name where the change should be.

So the first iteration of the fix was trivial. I also added a parameter block
for the relevant test case. But when I ran the test case
locally, it failed because the `color` option is converted from allowed string
values to Boolean.

Anthony was helpful in pointing out the code where color is "changed" from a
string to a Boolean.

## What I learnt

I learnt about pre-commit tool itself. It is an amazing tool. It automates
fixes bunch of things that should be fixed before the commit.

I also learnt about `color` option for `git diff`.

My emacs it set to use "black" formatter to **automatically fix** any style issues -
on each save. For some reason, pre-commit does not have "black" as a built-in
"fixer".

Since black would "fix" the entire file, the diff would show a lot more
changes than it should. So I had to use vim to make my changes. Since the
changes were only in a single function, it was OK.

(I could have used emacs like normal, and stage only relevant changes. Since I
had to go thru couple of commits, it was good that I just used vim from the terminal.)


## Thanks

(One of the) Maintainer of pre-commit - Anthony Sottile - was helpful.
It is important for new contributors (like myself) feel welcomed, and not
"judged".

He also runs twitch sessions regularly where he talks about his thought process
and also helps fellow contributors.

## The Pull Request

For your reference, you can see the PR itself [here](https://github.com/pre-commit/pre-commit/pull/1051)
