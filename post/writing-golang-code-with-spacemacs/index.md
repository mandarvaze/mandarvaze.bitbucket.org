
Since I use spacemacs, everything is easy.

As soon as I started editing a `.go` file, spacemacs
offered to enable the `go` layer. (I could have easily done
it myself, but it is always awesome when your editor "knows" and
offers to help.)

After saving the `.spacemacs` and `SPC f e R` later (and I think I had to reopen
the buffer, I don't remember) I had working syntax highlighting and "coding
time error checking" (Thanks `flycheck`)

This is when I noticed the "hard" tabs. See References below.

I was surprised to find out that `gofmt` tool uses tabs
(width=8) to format the `go` code. I quickly learnt that I
should not try to enforce use of spaces, instead I could set my own tab
width, the way I want.

So I added the following line to my `.spacemacs`

```lisp
(add-hook 'go-mode-hook (lambda() (setq go-tab-width 4)))
```

As I neared successfully completing my first `go` program,
I appreciated how complete the `golang` support is, in the
spacemacs go layer.

As soon as I save the file, the `gofmt` command is fires
up, and shows up any errors in the right hand side buffer (split window)

If there are no errors, the it formats the code as per the (go)
standards.

There are more `go` layer functionalities like :

-   `godoc` : Shows up the documentation. Works offline as
    well. I checked ;)
-   `go-import-add` : I'm yet to realize the usefulness of
    this, but seems nice.

You can send a buffer or a region to the "go playground"

Most of these are available via `M-x` command or via the
menu bar.

I may write part two of this, or just update this once I start using
`go` seriously

*Edit \[Jan 2, 2017\]*

As I started writing more `go` code, I felt that I need
auto completion, which is configured by default in the go layer, but was
broken, because I had not installed the external dependencies[^1]

Once I did that, auto-completion started working well.

I have not yet installed `gomtalinter`, mostly because I am
too new to `go` and I think the errors shown by default are
enough for me. In future, I might start using
`gomtalinter`.

For default keyboard shortcuts configured for the `go`
layer, see[^2]

------------------------------------------------------------------------
References:

* [Stack Overflow](http://stackoverflow.com/questions/19094704/indentation-in-go-tabs-or-spaces)
* [Google Groups](https://groups.google.com/forum/#!topic/golang-nuts/93t0LWvHrKM)
* [Google I/O 2012 : Meet the go Team](https://www.youtube.com/watch?v=sln-gJaURzk&t=1738)

[^1]: [Spacemacs go layer
    pre-requisites](https://github.com/syl20bnr/spacemacs/tree/master/layers/%2Blang/go#pre-requisites)

[^2]: [Spacemacs go
    commands](https://github.com/syl20bnr/spacemacs/tree/master/layers/%2Blang/go#working-with-go)
