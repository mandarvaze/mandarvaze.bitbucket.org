Found this via Insight Timer App.
Listen to the full audio [here](https://insighttimer.com/anabarreto/guided-meditations/7-opportunities-a-day-to-bring-your-best-self-forward)

<!--more-->

1. Gratitude : Start your day by finding 5 things to be grateful for. Followed by Guided meditation. Focus on breathing.
1. Self talk : When you brush your teeth, Look into a mirror and say positive things
1. Shower :  Use water to remove negative energy. Look up Masuru Imoto's work "Hidden messages in Water"
1. During commute : Additional 5 things to be grateful for.
1. Wish well to 3 people at work: Even if silent. This is called "Silent generosity"
1. During conflict: Nostral breathing. Close one nostril and breath thru one, then switch. Do this for 3-5 minutes. **Look for green objects in the room.**
1. End of the day: Find 5 things from the day to be grateful for. Wish for better tomorrow.
