
## Problem
When working on my recent project, I started getting the following error:

`module Q is not loaded and could not be found`

The error was shown when I run : `mix test`

## Troubleshooting

1. I confirmed that the module was listed in the `mix.exs` and was installed via
   `mix deps.get`
2. I tried `iex -S mix` but no error. In fact `Q.q(msg)` worked.

I turned to Local Elixir Telegram group. Someone suggested that it might be a
typo since the name of the module on hex (and in `mix.exs`) was `qq`, while I was
getting an error for the module `Q` (But that was not the case, I have [explained](https://github.com/mandarvaze/q/blob/master/README.md#why-name-the-hex-package-qq)
in the README of the qq package, about this discrepency)

Someone else shared
[this](https://elixirforum.com/t/load-module-during-test/7400) link.

It lead me to a possible problem/solution.

## Solution

`Q.q` is a "quick and dirty debugging tool". Needless to say it is needed (and
should be used) only during the development. So I had mentioned the dependency
as `only: :dev`

That is why it worked with `iex -S mix` (I think dev is default. But this is
just a hunch) and failed only for `mix test`

Changing it to `only: [:dev, :test]` resolved the issue.

I also learnt that even if some dependency is dev-only, it might be better to
list it as `only: [:dev]` - It could have been easier to identify the problem
(or may be not)

## Thanks

Thanks to people on my local Elixir Telegram group for helping me solve this.
Special thanks to Aditya aka BrainBuzzer for the link (mentioned above)
