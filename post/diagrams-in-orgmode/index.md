
Recently, I had to create a technical document for a project, and I started with `org-mode` since that was easiest.

Basic markup is easy in org, and was enough for me to get started.

In order to share with external parties, I exported to HTML. In past I had tried
`docx` but that did not work well, I wanted to export to PDF, but it requires
`pdflatex` which seemed a lot of hassle. I might do that later, but for now HTML
was acceptable.

While I started playing with headers, especially related to export, I came
across `ditaa`. It allows "text mode" diagrams into nice "gui mode" diagrams
(for the lack of better word**

Demo pages looked awesome, but as with all things, it did not work for the first time.

## `evaluation of this code block is disabled`

Removing `org-plus-contrib` and reinstalling helps resolve this issue.

I also had to specifically enable ditaa for org-babel (I'm not sure, whether
this is mandatory)

```
  (org-babel-do-load-languages
   'org-babel-load-languages
   '(
     (ditaa . t)
     (python . t)
     )
  )
```

## `Could not find ditaa.jar at ~/.emacs.d/elpa/contrib/scripts`

Once the original error went away, I ran into the above error. It is said that
it is shipped with org-mode, but I guess not (Or at least not with spacemacs
which I am using)

I downloaded latest ditaa (which comes in the form of a zip file) from
[here](https://sourceforge.net/projects/ditaa/)

Then manually copied `ditaa.jar` to `~/.emacs.d/elpa/contrib/scripts` (The last
two components did not exist, but I manually created them)

There is also another option. Add the following to your emacs config

```
(setq org-ditaa-jar-path "/path/to/ditaa.jar")
```

If and when I will need ditaa as a standalone program, I might switch to the
second option. Till then only emacs/org needs this, so I am OK copying it under
`.emacs.d`

Now, when I export to HTML, ditta block is evaluated ( I am asked for a
confirmation) and beautiful PNG file is shown in the HTML.

Nice.

## Demo

*Edit: As asked in the comments, here is a "show case" of what ditaa can do*

The following block of code in the `.org` file produces the output shown below.

```
#+begin_src ditaa :file diag.png

/---------\    +-----------------+    /--------\
|  cBLU   |    |                 |    |  {s}   |
|   GUI   |<-->| REST API Server |<-->| RDBMS  |
|         |    |                 |    |        |
\---------/    +-----------------+    \--------/
                        ^
                        :
                        |
                        v
                    /-------\
                    | {d}   |
                    | Files |
                    \-------/
#+end_src
```

{{< figure src="/images/blog/diag.png" >}}

In the sample above, I've tried to show case

* Normal rectangle
* Rectangle with rounded corners
* Colored box
* Dotted connector
* Storage
* Document

You can find more "samples" [here](http://ditaa.sourceforge.net/#usage)

--------

## Resources

* Read about ditaa [here](http://ditaa.sourceforge.net/#intro)
* Draw Class diagrams and/or sequence diagrams using PlantUML
