
If you use `org-mode` to keep track of your `TODO` lists, then this might be
useful.

Assuming you have tasks in an hierarchical manner, you should **Put \[/\] and/or
\[%\] in the header**

Use TODO keywords in the child node, as you normally would.

When an entry is marked `DONE`, values are updated
automatically.

In case you need to update the values manually, do `C-c C-c`

I needed this when I refiled the entries. They were not `DONE`, so automatic
update did not work.

Here is how it looks :

```shell
* Plan Trip to Paris [1/3] [33%]
 * DONE Book Tickets
 * TODO Pack your Bags
 * TODO Set vacation auto-response
```

You can read more
[here](http://orgmode.org/manual/Breaking-down-tasks.html)
