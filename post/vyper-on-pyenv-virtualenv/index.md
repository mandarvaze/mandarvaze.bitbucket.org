
Initially, setting up Vyper (an experimental language, with python-like syntax,
for writing smart contracts on ethereum) seemed as simple as following the
[instructions](http://viper.readthedocs.io/en/latest/installing-vyper.html#macos).

it wasn't.

*If you like to skip the details/troubleshooting, head over to [TL;DR](#tldr)*

-----

## pyenv

I've written about why I prefer pyenv [earlier]({{< relref
"post/pynev-and-virtualenv.md">}})

I'm not sure whether my "misfortune" was due to pyenv, but it could be.

To top it off, I also used `virtualenv` as suggested in the official docs. (I
would have done that any way)

So it added to complications, and "simple" instructions did not work.

I had discussion on gitter about this issue, and one person had also faced
problems. (But he wasn't using `virtualenv`)

## Problems and Solutions

It seemed that missing `requirements.txt` is the problem (and the in-progress documentation)
Several dependencies are not clearly listed.

First I ran into the following error, but the solution is well documented.

```bash
build/temp.macosx-10.12-x86_64-3.6/_openssl.c:493:10: fatal error: 'openssl/opensslv.h' file not found
```
So I had to set the `CFLAGS` and `LDFLAGS` variable, as mentioned on the official instructions page.

While troubleshooting, I realized that dependencies are badly broken.

I manually installed `py-evm` because it is listed in setup.py

**This installed a whole lot of packages correctly** which are required later.

e.g. [This](https://github.com/ltfschoen/vyper/blob/master/README_MAC.md#chapter-0)
troubleshooting guide, asks to install a specific version of `pypandoc`.

When I did that, I saw several errors of the nature `packageA requires pakcageB,
which is not installed.` Yet, at the end I saw `Successfully installed
pypandoc-1.4`

That does not make sense.

`pip install py-evm` I think installed whole lot of those *missing* packages
correctly.

(*Note: In my latest attempt to verify these instructions, `make` worked after this*)

Then I ran into error related to `pytest-runner`
```bash
distutils.errors.DistutilsError: Download error for https://files.pythonhosted.org/packages/9e/b7/fe6e8f87f9a756fd06722216f1b6698ccba4d269eac6329d9f0c441d0f93/pytest-runner-4.2.tar.gz#sha256=d23f117be39919f00dd91bffeb4f15e031ec797501b717a245e377aee0f577be: [SSL: WRONG_VERSION_NUMBER] wrong version number (_ssl.c:748)
```

Essentially it was trying to download pytest-runner, so first I tried curl using that URL (minus #sha256..) it worked. It dumped binary on the console. So there was no error reaching and downloading the file.
So I manually pip install pytest-runner==4.2
After which make worked. (make test still failing, because I'm getting same error, but for ethereum)

I think it may have been "one off" error, cause in later attempts, I did not face this issue.

-----

## TL;DR : Steps that worked {#tldr}

As mentioned in the [official documentation]
(http://viper.readthedocs.io/en/latest/installing-vyper.html#macos) I already
had python 3.6 and `gmp` installed via `brew`

```bash
pyenv virtualenv 3.6.2 py362vyper
pip install --upgrade pip
export CFLAGS="-I$(brew --prefix openssl)/include"
export LDFLAGS="-L$(brew --prefix openssl)/lib"
pip install scrypt
pip install py-evm==0.2.0a12
pip install pytest-runner==4.2
git clone https://github.com/ethereum/vyper.git
cd vyper
make
pip install ethereum==2.3.1
pip install pytest-cov==2.5.1
make test
vyper examples/crowdfund.v.py
```

*Note : In the steps above, I've listed a specific versions of various packages
 I installed manually. Eventually, It was not required. So try without
 mentioning specific version at first. If that does not lead to success, maybe
 try specific version - which may differ depending on when you are reading this*
