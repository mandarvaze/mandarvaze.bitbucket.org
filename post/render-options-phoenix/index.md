
When building a Hello World type program, I am interested in seeing the "route"
working.

During early development, it is easy to just render a text.

Most tutorials use `render(conn, "show.html")`- which requires one to create
view function, as well as `show.html.eex`

While this may be required later, there is an easy way 

`text(conn, "Hello #{user}")`

I had to *look* for it

Sometimes `json(conn, my_json)` may also be useful.

:smile:
