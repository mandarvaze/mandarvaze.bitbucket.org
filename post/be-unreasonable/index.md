
I came across [this
video](http://www.robinsharma.com/academy/yabyy/unleash-your-greatness-this-year/)
by Robin Sharma (The author of great books like "The Monk who sold his
Ferrari" and "The leader without title")

It starts with the following quote :

> The reasonable man adapts himself to the world; the unreasonable one
> persists in trying to adapt the world to himself. Therefore all
> progress depends on the unreasonable man.
>
> George Bernard Shaw

Robin goes on to suggest that you install 3 habits during the next 90
days, that set the tone for the next 12 months, so that 2016 can be your
greatest year (Rather than a repeat of the last year)

1.  Protect your mindset
    - Make sure you are surrounded by positive people
    - Don't watch TV
2.  Prepare the night before
    - Plan for the next day.
    - Write top three lessons learned that day
    - Write three positive things that happened today
3.  Focus on single biggest opportunity

> A Person who chases two rabbits, catches neither
>
> Confucius

------------------------------------------------------------------------

*Note:* I have been writing the [Five Minute
Journal](http://www.fiveminutejournal.com/) last 6 months. It covers the
second point very well.
