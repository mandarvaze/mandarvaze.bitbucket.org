How a "request" moves thru various phoenix layers

<!--more-->

``` elixir
connection
|> endpoint()
|> router()
|> pipelines()
|> controller()
```
The `connection` comes in with information about the request.
When Phoenix is done, that `connection` will have the response in it.


*From the book "Programming Phoenix 1.4"*
