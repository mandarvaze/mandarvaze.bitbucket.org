
These are the sketch notes.

Digital ones were created on Samsung Galaxy 8, using Autodesk Sketch Express (and Autodesk Sketchbook)
Exported using Airdroid, and did minor post-processing on OSX (preview app)

Later, I have drawn several using good old pen-and-paper (and were scanned)

I am grateful to
[Sacha Chua](http://sachachua.com/blog/tag/sketchnote-lessons/)
for the inspiration.

Thanks [Sunni
Brown](http://sunnibrown.com/doodlerevolution/bootcamps/visual-notetaking-101/)
for the Visual Notetaking 101 course.

**Click on the following sketch, to see others**
<!--more-->

{{< gallery >}}
    {{< figure link="/images/sketchnotes/30daysOfGenius-AustinKleon.png" caption="30 Days of Genius : Austin Kleon" >}}
    {{< figure link="/images/sketchnotes/vn101-practice2.png" caption="Visual Notes 101: Practice" >}}
    {{< figure link="/images/sketchnotes/vn101-practice1.png" caption="Visual Notes 101: Practice" >}}
    {{< figure link="/images/sketchnotes/vn101-practice3.png" caption="Visual Notes 101: Practice" >}}
    {{< figure link="/images/sketchnotes/sketchnotes-practice.png" caption="Sketchnotes Practice" >}}
    {{< figure link="/images/sketchnotes/icon-lib.png" caption="Icon Library" >}}
    {{< figure link="/images/sketchnotes/snday2016.png" caption="Sketchnotes Day 2016" >}}
    {{< figure link="/images/sketchnotes/Digital_sketchnotes_pros_cons.jpg" caption="Why Sketchnotes" >}}
    {{< figure link="/images/sketchnotes/how_2_improve_sketchnotes.jpg" caption="Improve Sketchnotes" >}}
    {{< figure link="/images/sketchnotes/Morning_Routine.jpg" caption="Morning Routine" >}}
    {{< figure link="/images/sketchnotes/Grateful.jpg" caption="Grateful" >}}
    {{< figure link="/images/sketchnotes/Plans_2016.png" caption="Plans for 2016" >}}
{{< /gallery >}}

{{< load-photoswipe >}}
