
Initially ...

These are the sketches I did from time to time, mostly for my son.
These are sketches of the images found in his books.

As you can see, these were drawn any almost any empty piece of paper (hence no
consistency in the background, size etc.) using a pencil.

Update :

Since I started drawing relatively consistently, I started drawing more
deliberately.

I got a re-writable notebook, and I have since drawn several
sketches in that, rather than "some piece of paper found at that moment" (I
occasionally still do that, but less so.)

I also started scanning these, rather than using mobile phone's camera to
capture these, for better quality. It helped a little.

I also started drawing digitally on a tablet. This created better images, no
grey "shadows" on the sides, for example.

But some how it does not feel the same as using "pen and paper". But I'm getting
better.

and off course I get "undo", which I can't on "pen and paper" (I wonder if that
is a good thing)

The drawings are still mostly from children's books and magazines. But I started
drawing from local newspaper as well.

**Click on the following sketch, to see others**
<!--more-->

{{< gallery >}}
    {{< figure link="images/sketches/be_happy.jpg" caption="Be Happy" >}}
    {{< figure link="images/sketches/girl-drawing.jpg" caption="Girl Drawing" >}}
    {{< figure link="images/sketches/good_morning.jpg" caption="Good Morning" >}}
    {{< figure link="images/sketches/love_your_family.jpg" caption="Love Your Family" >}}
    {{< figure link="images/sketches/palm_tree.jpg" caption="Palm Tree" >}}
    {{< figure link="images/sketches/man.jpg" caption="Man" >}}
    {{< figure link="images/sketches/landscape.jpg" caption="Landscape" >}}
    {{< figure link="images/sketches/shivaji.jpg" caption="Shivaji" >}}
    {{< figure link="images/sketches/gandhiji.jpg" caption="Gandhiji" >}}
    {{< figure link="images/sketches/baloo.jpg" caption="Baloo" >}}
    {{< figure link="images/sketches/ganesh.jpg" caption="Ganesh" >}}
    {{< figure link="images/sketches/kaa.jpg" caption="kaa" >}}
    {{< figure link="images/sketches/mowgli.jpg" caption="Mowgli" >}}
    {{< figure link="/images/sketches/monkey.jpg" caption="Monkey" >}}
    {{< figure link="/images/sketches/deer.jpg" caption="Deer" >}}
    {{< figure link="/images/sketches/hippo.jpg" caption="Hippo" >}}
    {{< figure link="/images/sketches/elephant4.jpg" caption="Elephant" >}}
    {{< figure link="/images/sketches/cute_tiger.jpg" caption="Tiger" >}}
    {{< figure link="/images/sketches/baby_elephant.jpg" caption="Elephant" >}}
    {{< figure link="/images/sketches/horse.jpg" caption="Horse" >}}
    {{< figure link="/images/sketches/rooster.jpg" caption="Rooster" >}}
    {{< figure link="/images/sketches/goat.jpg" caption="Goat" >}}
    {{< figure link="/images/sketches/doggy2.jpg" caption="Doggy" >}}
    {{< figure link="/images/sketches/doggy.jpg" caption="Doggy" >}}
    {{< figure link="/images/sketches/animals2.png" caption="Animals" >}}
    {{< figure link="/images/sketches/dog3.jpg" caption="Dog" >}}
    {{< figure link="/images/sketches/art-of-doodling.png" caption="Art of Doodling" >}}
    {{< figure link="/images/sketches/water.jpg" caption="Water Is Life" >}}
    {{< figure link="/images/sketches/windows.png" caption="Windows" >}}
    {{< figure link="/images/sketches/elephant3.jpg" caption="Elephant" >}}
    {{< figure link="/images/sketches/croc.jpg" caption="Crocodile" >}}
    {{< figure link="/images/sketches/arlo.jpg" caption="Arlo" >}}
    {{< figure link="/images/sketches/ccd.jpg" caption="Urban Sketching - CCD" >}}
    {{< figure link="/images/sketches/niwara.jpg" caption="Urban Sketching - Niwara" >}}
    {{< figure link="/images/sketches/using-ball-point-pen.jpg" caption="Ball Point Pen" >}}
    {{< figure link="/images/sketches/pencil-shading4.jpg" caption="Pencil Shading" >}}
    {{< figure link="/images/sketches/pencil-shading1.jpg" caption="Pencil Shading" >}}
    {{< figure link="/images/sketches/pencil-shading2.jpg" caption="Pencil Shading" >}}
    {{< figure link="/images/sketches/pencil-shading3.jpg" caption="How to Hold a Pencil" >}}
    {{< figure link="/images/sketches/cartoon-faces.png" caption="Faces" >}}
    {{< figure link="/images/sketches/tiger2.jpg" caption="Tiger (in the) Woods" >}}
    {{< figure link="/images/sketches/flounder.jpg" caption="Flounder" >}}
    {{< figure link="/images/sketches/bravo.jpg" caption="Bravo" >}}
    {{< figure link="/images/sketches/crow.png" caption="Crow" >}}
    {{< figure link="/images/sketches/vitthal.png" caption="Vitthal" >}}
    {{< figure link="/images/sketches/tiger.png" caption="Tiger" >}}
    {{< figure link="/images/sketches/ant.png" caption="Ant" >}}
    {{< figure link="/images/sketches/election.png" caption="Elections" >}}
    {{< figure link="/images/sketches/mandeshi.png" caption="People" >}}
    {{< figure link="/images/sketches/shiva.png" caption="Shiva" >}}
    {{< figure link="/images/sketches/little-einsteins1.png" caption="Little Einsteins" >}}
    {{< figure link="/images/sketches/dog3.png" caption="Dog" >}}
    {{< figure link="/images/sketches/relatives1.png" caption="Relatives" >}}
    {{< figure link="/images/sketches/relatives2.png" caption="Relatives" >}}
    {{< figure link="/images/sketches/relatives3.png" caption="Relatives" >}}
    {{< figure link="/images/sketches/r_k_laxman.png" caption="R K Laxman" >}}
    {{< figure link="/images/sketches/dada.jpg" caption="Boy wearing Ear Muffs" >}}
    {{< figure link="/images/sketches/tai.jpg" caption="Tai (Elder Sister)" >}}
    {{< figure link="/images/sketches/timoy.jpg" caption="Timoy" >}}
    {{< figure link="/images/sketches/sketch_at_yolkshire.jpg" caption="Yolkshire" >}}
    {{< figure link="/images/sketches/tedxhull_graham_shaw2.jpg" caption="Anyone can draw - Graham Shaw" >}}
    {{< figure link="/images/sketches/tedxhull_graham_shaw1.jpg" caption="Anyone can draw - Graham Shaw" >}}
    {{< figure link="/images/sketches/montu.jpg" caption="Montu" >}}
    {{< figure link="/images/sketches/buddha.png" caption="Buddha" >}}
    {{< figure link="/images/sketches/bunny2.jpg" caption="Bunny" >}}
    {{< figure link="/images/sketches/padgaonkar.png" caption="Padgaonkar" >}}
    {{< figure link="/images/sketches/dog2.jpg" caption="Dog" >}}
    {{< figure link="/images/sketches/owl.png" caption="Owl" >}}
    {{< figure link="/images/sketches/elephant2.png" caption="Elephant" >}}
    {{< figure link="/images/sketches/elephant.png" caption="Elephant" >}}
    {{< figure link="/images/sketches/dog.jpg" caption="Dog" >}}
    {{< figure link="/images/sketches/girl.jpg" caption="Girl" >}}
    {{< figure link="/images/sketches/boy.jpg" caption="Boy" >}}
    {{< figure link="/images/sketches/cat_and_dogs.png" caption="Cat and Dogs" >}}
    {{< figure link="/images/sketches/birds.png" caption="Birds" >}}
    {{< figure link="/images/sketches/animals.png" caption="Animals" >}}
    {{< figure link="/images/sketches/bunny.jpg" caption="Bunny" >}}
{{< /gallery >}}

{{< load-photoswipe >}}

<!--
    "/images/sketches/monkey.jpg"
    "/images/sketches/deer.jpg"
    "/images/sketches/hippo.jpg"
    "/images/sketches/elephant4.jpg"
    "/images/sketches/cute_tiger.jpg"
    "/images/sketches/baby_elephant.jpg"
    "/images/sketches/horse.jpg"
    "/images/sketches/rooster.jpg"
    "/images/sketches/goat.jpg"
    "/images/sketches/doggy2.jpg"
    "/images/sketches/doggy.jpg"
    "/images/sketches/animals2.png"
    "/images/sketches/dog3.jpg"
    "/images/sketches/art-of-doodling.png"
    "/images/sketches/water.jpg"
    "/images/sketches/windows.png"
    "/images/sketches/elephant3.jpg"
    "/images/sketches/croc.jpg"
    "/images/sketches/arlo.jpg"
    "/images/sketches/ccd.jpg"
    "/images/sketches/niwara.jpg"
    "/images/sketches/using-ball-point-pen.jpg"
    "/images/sketches/pencil-shading4.jpg"
    "/images/sketches/pencil-shading1.jpg"
    "/images/sketches/pencil-shading2.jpg"
    "/images/sketches/pencil-shading3.jpg"
    "/images/sketches/cartoon-faces.png"
    "/images/sketches/tiger2.jpg"
    "/images/sketches/flounder.jpg"
    "/images/sketches/bravo.jpg"
    "/images/sketches/crow.png"
    "/images/sketches/vitthal.png"
    "/images/sketches/tiger.png"
    "/images/sketches/ant.png"
    "/images/sketches/election.png"
    "/images/sketches/mandeshi.png"
    "/images/sketches/shiva.png"
    "/images/sketches/little-einsteins1.png"
    "/images/sketches/dog3.png"
    "/images/sketches/relatives1.png"
    "/images/sketches/relatives2.png"
    "/images/sketches/relatives3.png"
    "/images/sketches/r_k_laxman.png"
    "/images/sketches/dada.jpg"
    "/images/sketches/tai.jpg"
    "/images/sketches/timoy.jpg"
    "/images/sketches/sketch_at_yolkshire.jpg"
    "/images/sketches/tedxhull_graham_shaw2.jpg"
    "/images/sketches/tedxhull_graham_shaw1.jpg"
    "/images/sketches/montu.jpg"
    "/images/sketches/buddha.png"
    "/images/sketches/bunny2.jpg"
    "/images/sketches/padgaonkar.png"
    "/images/sketches/dog2.jpg"
    "/images/sketches/owl.png"
    "/images/sketches/elephant2.png"
    "/images/sketches/elephant.png"
    "/images/sketches/dog.jpg"
    "/images/sketches/girl.jpg"
    "/images/sketches/boy.jpg"
    "/images/sketches/cat_and_dogs.png"
    "/images/sketches/birds.png"
    "/images/sketches/animals.png"
    "/images/sketches/bunny.jpg"
-->
