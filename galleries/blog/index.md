
These are the sketches used in my blog posts.

I am grateful to
[Darius Foroux](http://http://dariusforoux.com) for the inspiration

These sketches are meant to be referenced in the various blog posts, and not
meant to be viewed as a gallery.

<!--more-->
{{< gallery >}}
    {{< figure link="images/blog/prize-linked-savings.png" caption="Prize Linked Savings" >}}
{{< /gallery >}}

{{< load-photoswipe >}}
