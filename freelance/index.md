
I am available for remote freelance assignments.

# Profile Links

* [Github](https://github.com/mandarvaze/)
* [Twitter](https://twitter.com/mandarvaze)
* [Linkedin](https://www.linkedin.com/in/mandarvaze)
* [Stackoverflow](https://stackoverflow.com/users/154947/mandar-vaze)
* [Digital Moleskine](https://wiki.desipenguin.com)
* [My other blog](https://Writings.desipenguin.com)

You can reach me on Twitter or LinkedIn

---------

# Projects

## Corporate Taxi Hiring platform

**Client : Ace Cabs India Pvt. Ltd.**

###  Features

* GPS enabled billing
* Instant Bill on SMS and Email
* mini map of the route on the Invoice
* Support for Multiple Operators (Multi Tenant Support)
* Support for different tariff Per Vehicle Type, Per City, Per Client
* Support for multiple billing entities for single Corporate Client
* Support for different GST rates per state (CGST, SGST, IGST)

### Revenue

Within 1 year of operation, we generated :

* 10000+ Invoices
* **Rs. 1.8 Cr** revenue

###  During Beta Trial

* Simultaneous 200 rides
* 4 cities
* 2 Corporate Clients 

###  Technology

* Backend : Python 3, flask (eve framework) for REST API server
  * Testing : pytest
  * Documentation : Sphinx
* Database : Postgres
* PaaS : Heroku
* Android : Java
* Web : Angular

###  My Role

I met with the stake holders to understand the requirements.
We jointly created the roadmap.

I then designed the system. I was the Architect and the Backend Engineer.
I had to create enough API endpoints such that frontend and Android development
could start.

Unit and integration tests came in handy to ensure that I could test the
functionality independently.

I later deployed the server to heroku. We had considered AWS, but Heroku is best
for the developer productivity, Especially when you don't have a dedicated
DevOps person.

We did a regular backups via pg_dump

###  Real world Fun Facts

* This was a "traditional" corporate taxi company, where the drivers were not
trained to use the "app" to start/stop the trips, so they would forget ;) (Ask
me in person, how we "handled" it)
* The trips lasted 8-12 hours, where drivers - when on a break - would walk with
the phone, causing "erroneous" readings.
* Various parts of town would have patchy or non existent cell coverage.
* GST was introduced after 6 months in production
* Demonetization was announced the night I was flying to Delhi for "Go Live" testing.
