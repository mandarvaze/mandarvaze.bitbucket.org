
Recently ran across this *unintuitive* error :

```
$ truffle test
/usr/local/lib/node_modules/truffle/build/cli.bundled.js:128717
var Module;if(!Module)Module=(typeof Module!=="undefined"?Module:null)||{};var moduleOverrides={};for(var key in Module){if(Module.hasOwnProperty(key)){moduleOverrides[key]=Module[key]}}var ENVIRONMENT_IS_WEB=false;var ENVIRONMENT_IS_WORKER=false;var ENVIRONMENT_IS_NODE=false;var ENVIRONMENT_IS_SHELL=false;if(Module["ENVIRONMENT"]){if(Module["ENVIRONMENT"]==="WEB"){ENVIRONMENT_IS_WEB=true}else if(Module["ENVIRONMENT"]==="WORKER"){ENVIRONMENT_IS_WORKER=true}else if(Module["ENVIRONMENT"]==="NODE"){ENVIRONMENT_IS_NODE=true}else if(Module["ENVIRONMENT"]==="SHELL"){ENVIRONMENT_IS_SHELL=true}else{throw new Error("The provided Module['ENVIRONMENT'] value is not valid. It must be one of: WEB|WORKER|NODE|SHELL.")}}else{ENVIRONMENT_IS_WEB=typeof window==="object";ENVIRONMENT_IS_WORKER=typeof importScripts==="function";ENVIRONMENT_IS_NODE=typeof process==="object"&&"function"==="function"&&!ENVIRONMENT_IS_WEB&&!ENVIRONMENT_IS_WORKER;ENVIRONMENT_IS_SHELL=!

TypeError: Cannot read property 'filter' of undefined
    at /usr/local/lib/node_modules/truffle/build/cli.bundled.js:318499:21
    at /usr/local/lib/node_modules/truffle/build/cli.bundled.js:267618:25
    at FSReqWrap.oncomplete (fs.js:166:21)
```

It took a bit of googling to figure out that error is far more innocent than it appears from the error and the stack trace.

It is simply the case of "File not found"

In my case, I had my tests under folder called `tests`, apparently truffle wants the one without the last `s`

See the details [here](https://github.com/trufflesuite/truffle/issues/837)

Apparently this has been fixed, but I'm unsure whether it made to any of the truffle releases.
