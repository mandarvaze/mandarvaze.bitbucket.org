
This blog started as a way to document (mainly for myself) things I
learned. I listen to podcasts during my commute. It is a great way to
make the (long?) commute enjoyable.

But the problem was that I could not take quick notes while driving. So
I made a point to jot down whatever I could as soon as I reach the
destination. At first I noted it down into evernote, and then formatted
it for this blog.

But after a while, I got busy and didn't get time to prettify my notes.

Then after few months, I came across [Sacha Chua's
Blog](http://sachachua.com/blog) where she talks about :

-   [No excuses blogging](https://gumroad.com/l/no-excuses-blogging)
-   [How to clear
    drafts](http://sachachua.com/blog/2015/02/clear-out-your-drafts-by-scheduling-minimum-viable-posts/)

So once again I have decided to blog more often without spending too
much time beautifying the post (and focusing more on contents)

------------------------------------------------------------------------

Recently, I came across [this
post](https://thraxys.wordpress.com/2016/01/03/each-1-teach-1/) which
talks about "lowering the psychological barrier to writing, by focusing
on short, simple, technical posts, rather then essays"

It resonates with my thoughts ;)

-------------------------------------------


## Why Do I have Galleries here ?

I agree, that based on what I said above, Galleries do not belong here.

Reason is simple. I initially started using Nikola as my blogging engine (SSG).
It had decent support for the galleries. Around the same time, I started
sketching, and needed a place "of my own" to display those.

Yes, I also have instagram account, but I have been bitten too many times when
the only source was a third party site, that went away.

No, I don't think instagram will go away soon. Too much in the future ? Who knows.

But why take chance ? Better (also) host it on your own site.

